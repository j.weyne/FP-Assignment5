module Views.BaseView (baseView) where

import Framework.Graphics (img, scale, line, color, translate, red)
import Framework.Types (View(..), Picture)
import Models.State

baseView = View {
    fview = \s -> do
        v1 <- imageView s
        v2 <- lineView
        return $ v1 `mappend` v2
}

lineView :: IO Picture
lineView = return $ translate 300.0 0.0 $ color red $ line [(x, y) | x <- [1 .. 10], y <- [1 .. 100]]

imageView :: State -> IO Picture
imageView state = return $ scale 0.25 0.25 (dirt $ tile $ file state)