module Views.GUIView where

import Framework
import Models.State
import Data.Monoid

view = View {
    fview = guiView
}

guiView :: State -> IO Picture
guiView state = return $ Pictures $ map piece $ hud $ world state

-- Render the tilegrid
renderHUDelements :: [[GUI_element]] -> Picture
renderHUDelements []       = blank
renderHUDelements (x:xs)   = renderLayer x <> (translate 0 (-40) $ renderHUDelements xs)
    where
        renderLayer  []     = blank
        renderLayer  (y:ys) = y <> ( translate 40 0 $ renderLayer ys )

scaleHUD :: [[GUI_element]] -> [[GUI_element]]
scaleHUD  = map (map $ scale 1 1)