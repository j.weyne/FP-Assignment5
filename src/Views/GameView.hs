module Views.GameView where

import Framework
import Models.State
import Views.GUIView as GUI
import Data.Fixed

gameView = View {
    fview = \s -> do
        v1 <- Views.GameView.view s
        v2 <- (fview GUI.view) s
        return $ v1 `mappend` v2
}

view :: State -> IO Picture
view state = return $ translate (-sx) (-sy - 64) $ renderGrid tiles `mappend` renderObjects (objects $ world state)
    where
        sx = fromIntegral (fst s)
        sy = fromIntegral (snd s)
        s  = coordsToScreen $ viewloc $ world state
        tiles = renderableTiles $ mapObjs (tilegrid $ world state) (objects $ world state)

data MappedTileGrid =
    MappedTileGrid {
        mwidth  :: Int,
        mheight :: Int,
        mgrid   :: [[RenderTile]]
    }


mapObjs :: TileGrid -> [GObject] -> MappedTileGrid
mapObjs g objs = MappedTileGrid (width g) (height g) (newgrid)
    where 
        tile     t o = RenderTile t o
        objAt  x_ y_ o = (x_ == (floor $ fst $ pos o)) && (y_ == (floor $ snd $ pos o))
        tiles      = [tile (grid g !! y !! x) (filter (objAt x y) objs) | x <- [0..width g - 1], y <- [0..height g - 1]]
        newgrid    = [take (width g) . drop (y * height g) $ tiles | y <- [0..height g - 1]]

data RenderTile =
    RenderTile {
        rimg :: Tile,
        objs :: [GObject]
    }

-- Convert square tilegrid to tilegrid ordered by diagonal, starting in upper left corner
renderableTiles :: MappedTileGrid -> [[RenderTile]]
renderableTiles t = map (getDiagonal $ mgrid t) [0..diagsAmount]
    where
        diagsAmount = mwidth t + mheight t - 2
        defaultTile t = RenderTile t []
        getDiagonal []     _ = []
        getDiagonal (x:_)  0 = [head x]
        getDiagonal (x:xs) n | n < length x = x!!n              : getDiagonal xs (n-1) 
                             | otherwise    = defaultTile blank : getDiagonal xs (n-1)

-- Render the tilegrid
renderGrid :: [[RenderTile]] -> Picture
renderGrid []     = blank
renderGrid (z:zs) = renderRow z `mappend` (translate 128 (-64) $ renderGrid zs)
    where
        renderRow  []     = blank
        renderRow  (x:xs) = rimg x `mappend` (translate (-256) 0 $ renderRow xs) --`mappend` (mconcat $ map (\o -> translate (fromIntegral.fst.screenObjPos $ o) (fromIntegral.snd.screenObjPos $ o) . draw $ o) (objs x))
        --screenObjPos o = coordsToScreen ((fst $ pos $ o) `mod'` 1 - 0.5, (snd $ pos $ o) `mod'` 1 - 0.5)

renderObjects :: [GObject] -> Picture
renderObjects l = mconcat $ map (\o -> translate (fromIntegral.fst.screenObjPos $ o) (fromIntegral.snd.screenObjPos $ o) . draw $ o) l
    where screenObjPos o = coordsToScreen ((fst $ pos $ o) `mod'` 1 - 0.5, (snd $ pos $ o) `mod'` 1 - 0.5)