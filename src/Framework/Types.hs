module Framework.Types (
    module Graphics.Gloss.Data.Picture,
    module Graphics.Gloss.Data.Vector,
    Event(..),
    Controller(..),
    View(..),
    ControllerReturn(..)) where

import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Vector
import Graphics.Gloss.Interface.Pure.Game

data Controller s =
    Controller {
        fstep :: Float -> s -> IO (ControllerReturn s),
        fevnt :: Event -> s -> IO (ControllerReturn s)
    }

newtype View s = 
    View {
        fview :: s -> IO Picture
    }

data ControllerReturn s =
    ReturnCtrl              (ControllerReturn s) |
    GiveCtrl (Controller s) (ControllerReturn s) |
    SetCtrl  (Controller s) (ControllerReturn s) |
    In       (View s)       (ControllerReturn s) |
    Show     s

instance Monoid (View s) where
    mempty      = View { fview = \_ -> return blank }
    mappend x y = View { fview = \d -> do
        a <- fview x d
        b <- fview y d
        return $ Pictures [a, b]
    }

{- TODO: Design job system
data Job = PureJob   [PureJob]   (Float -> GameState -> IO (GameState -> GameState, [Job])) |
           ImpureJob [ImpureJob] (Event -> GameState -> IO (GameState -> GameState, [Job]))
           -}