module Framework.Game.Types (
    Speed(..),
    Position(..),
    GameObject, load, save,
    InGame, step
) where

import Framework.Types (Picture, Vector(..))

-- Types
type Speed       = Vector
type Position    = Vector

-- Requires instances to be drawable to the screen, possibly with an offset
class Drawable a where
    draw :: Position -> a -> Picture

-- Requires instances to be serializble
class GameObject a where
    load :: String -> a
    save :: a -> String

-- Requires instances, whom are both a GameObject as well as a Drawable, to be updatable in the gameloop
class (GameObject a, Drawable a) => InGame a where
    step :: Float -> a -> a

-- pos a -> Position Vector and speed a -> Speed Vector
class (InGame a) => Movable a where
    pos :: a -> Position