module Framework.Graphics (
    module Graphics.Gloss.Data.Picture, 
    module Graphics.Gloss.Data.Color,
    img) where

import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Juicy (loadJuicy)
import System.IO
import Data.Maybe(fromMaybe)

img :: FilePath -> IO Picture
img fname = loadJuicy fname >>= return . fromMaybe (color red $ text "IMG ERROR")