module Framework.Router (
    routeStep,
    routeEvent,
    routeView,
    MVCState(..)) where

import Framework.Types

data MVCState s =
    MVCState {
        model      :: s,
        view       :: View s,
        controller :: [Controller s]
    }

routeStep :: Float -> MVCState s -> IO (MVCState s)
routeStep f s = do
    returnval <- (fstep $ head $ controller s) f (model s)
    return $ getGameState s returnval

routeEvent :: Event -> MVCState s -> IO (MVCState s)
routeEvent e s = do
    returnval <- (fevnt $ head $ controller s) e (model s)
    return $ getGameState s returnval

routeView :: MVCState s -> IO Picture
routeView s = (fview $ view s) (model s)

-- Takes the old MVCState, overrides fields as specified by ControllerReturn, and returns updated MVCState
getGameState :: MVCState s -> ControllerReturn s -> MVCState s
getGameState o (ReturnCtrl   c) = n { controller =   zs } where n = getGameState o c; (_:zs) = controller n
getGameState o (GiveCtrl   x c) = n { controller = x:ys } where n = getGameState o c;    ys  = controller n
getGameState o (SetCtrl    x c) = n { controller = x:zs } where n = getGameState o c; (_:zs) = controller n
getGameState o (In         x c) = n { view       = x }    where n = getGameState o c
getGameState o (Show       x)   = o { model      = x }