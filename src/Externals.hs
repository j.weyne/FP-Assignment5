module Externals (Tiles(..), GUI(..), UnitSprites(..), UnitModels(..), ExternalFiles(..), generateFiles) where

import Framework.Graphics
import System.Directory
import Data.List

data ExternalFiles = ExternalFiles {
    tile   :: Tiles,
    gui    :: GUI,
    units  :: UnitModels
} deriving (Show)

data Tiles = Tiles {
    dirt  :: Picture,
    rock  :: Picture,
    snow  :: Picture,
    stone :: Picture
} deriving (Show)

data GUI = GUI {
    hud1  :: Picture
} deriving (Show)

data UnitModels = UnitModels {
    basic :: UnitSprites
} deriving (Show)

data UnitSprites = UnitSprites {
    _E  :: Picture,
    _N  :: Picture,
    _NE :: Picture,
    _NW :: Picture,
    _S  :: Picture,
    _SE :: Picture,
    _SW :: Picture,
    _W  :: Picture
} deriving (Show)

--Perhaps make a mini DSL out of this instead?
data GUIelements = GUIelements {
    --borders
    borderDown        :: Picture,
    borderLeft        :: Picture,
    borderRight       :: Picture,
    borderUp          :: Picture,
    --buttons
    buttonLeft        :: Picture,
    buttonRight       :: Picture,
    --Corners
    cornerRightUp     :: Picture,
    cornerRightDown   :: Picture,
    cornerLeftDown    :: Picture,
    cornerLeftUp      :: Picture,
    --Small Ornaments
    ornLeftDown       :: Picture,
    ornRightDown      :: Picture,
    ornRightUp        :: Picture,
    ornLeftUp         :: Picture,
    ornRightTop       :: Picture,
    ornLeftTop        :: Picture,
    ornRightBottom    :: Picture,
    ornLeftBottom     :: Picture,
    --Big Ornaments
    bigOrnLeftUp      :: Picture,
    bigOrnRightUp     :: Picture,
    bigOrnRightDown   :: Picture,
    bigOrnLeftDown    :: Picture,
    bigOrnLeftTop     :: Picture,
    bigOrnRightTop    :: Picture,
    bigOrnLeftBottom  :: Picture,
    bigOrnRightBottom :: Picture,
    --Misc
    fill              :: Picture,
    title             :: Picture
} deriving (Show)

data HUD_COLOUR = Black | Blue | Cyan | Green | Purple | Red | White | Yellow
    deriving (Show)


generateFiles :: IO ExternalFiles
generateFiles = do
    --Tiles
    loadT <- loadTiles
    let tiles = initTiles loadT
    --Gui
    loadG <- loadGUI
    let gui = initGUI loadG
    --UnitSprites 
    loadUs <- loadUnitSprites
    let units = initUnitModels $ map initUnitSprites loadUs

    -- File structure
    return ExternalFiles {
        tile = tiles,
        gui = gui,
        units = units
    }

--Functions to load files

loadTiles :: IO [Picture]
loadTiles = do
    -- Tiles
    a  <- img "../../../src/Externals/Tiles/dirt.png"
    b  <- img "../../../src/Externals/Tiles/rock.png"
    c  <- img "../../../src/Externals/Tiles/snow.png"
    d  <- img "../../../src/Externals/Tiles/stone.png"
    return $ [a, b, c, d]

loadGUI :: IO [Picture]
loadGUI = do
    g1 <- img "../../../src/Externals/Space-Gui/HudBlack.png"
    return [g1]

loadGUIelements   :: HUD_COLOUR -> IO [Picture]
loadGUIelements c  = do
    let dir = "../../../src/Externals/Space-Gui/" ++ show c ++ "/"
    fnames' <- getDirectoryContents dir
    let fnames = reverse $ init $ init fnames'    -- remove last two entries as these are "." and ".."
    writeFile "E:/Repos/FP-Ass/errors2.txt" $ intercalate "\t" fnames
    mapM (\fp -> img $ dir ++ fp) fnames

loadUnitSprites :: IO [[Picture]]
loadUnitSprites = do
    e  <- img "../../../src/Externals/Units/spaceCraft1_E.png"
    n  <- img "../../../src/Externals/Units/spaceCraft1_N.png"
    ne <- img "../../../src/Externals/Units/spaceCraft1_NE.png"
    nw <- img "../../../src/Externals/Units/spaceCraft1_NW.png"
    s  <- img "../../../src/Externals/Units/spaceCraft1_S.png"
    se <- img "../../../src/Externals/Units/spaceCraft1_SE.png"
    sw <- img "../../../src/Externals/Units/spaceCraft1_SW.png"
    w  <- img "../../../src/Externals/Units/spaceCraft1_W.png"
    return [[e, n, ne, nw, s, se, sw, w]]

--Functions to put elements into respective record fields

initTiles :: [Picture] -> Tiles
initTiles (a:b:c:d:xs) = Tiles {
        dirt  = a,
        rock  = b,
        snow  = c,
        stone = d
    }

initGUI :: [Picture] -> GUI
initGUI (a:[]) = GUI {
    hud1 = a
}

initGUIelements   :: [Picture] -> GUIelements
initGUIelements [
    --borders
    bDown,
    bLeft,
    bRight,
    bUp,
    --buttons
    butL,
    butR,
    --corners
    corRup,
    corRdown,
    corLdown,
    corLup,
    --fill
    fill_,
    --Small Ornaments
    oLdown,
    oRdown,
    boRdown,
    boLdown,
    boLtop,
    boRtop,
    boLbottom,
    boRbottom,
    oRup,
    oLup,
    oRtop,
    oLtop,
    oRbottom,
    oLbottom,
    --Big Ornaments
    boLup,
    boRup,
    --title
    title_ ] = GUIelements {
       --borders
       borderDown        = bDown,
       borderLeft        = bLeft,
       borderRight       = bRight,
       borderUp          = bUp,
       --buttons
       buttonLeft        = butL,
       buttonRight       = butR,
       --Corners
       cornerRightUp     = corRup,
       cornerRightDown   = corRdown,
       cornerLeftDown    = corLdown,
       cornerLeftUp      = corLup,
       --Small Ornaments
       ornLeftDown       = oLdown,
       ornRightDown      = oRdown,
       ornRightUp        = oRup,
       ornLeftUp         = oLup,
       ornRightTop       = oRtop,
       ornLeftTop        = oLtop,
       ornRightBottom    = oRbottom,
       ornLeftBottom     = oLbottom,
       --Big Ornaments
       bigOrnLeftUp      = boLup,
       bigOrnRightUp     = boRup,
       bigOrnRightDown   = boRdown,
       bigOrnLeftDown    = boLdown,
       bigOrnLeftTop     = boLtop,
       bigOrnRightTop    = boRtop,
       bigOrnLeftBottom  = boLbottom,
       bigOrnRightBottom = boRbottom,
       --Misc
       fill              = fill_,
       title             = title_
}

initUnitSprites :: [Picture] -> UnitSprites
initUnitSprites (e:n:ne:nw:s:se:sw:w:[]) = UnitSprites {
    _E  = e,
    _N  = n,
    _NE = ne,
    _NW = nw,
    _S  = s,
    _SE = se,
    _SW = sw,
    _W  = w
} 

initUnitModels :: [UnitSprites] -> UnitModels
initUnitModels (b:[]) = UnitModels {
    basic = b
}




{-
{-The Folowing code uses the Control.Monad.Trans.State methods
to parse a list into a record; Inspired by this solution: 
https://stackoverflow.com/questions/15734573/initializing-algebraic-data-type-from-list-}

foo :: StateT [Picture] Maybe GUI
foo = do
    a <- get
    case a of
        []     -> Nothing
        (x:xs) -> do
            put xs
            return x

bar :: StateT [Picture] Maybe ()
bar = do
    b <- get
    case s of
        []  -> return ()
        _   -> Nothing

parse :: [Picture] -> Maybe GUI
parse l = evalStateT $ GUI
                <$> foo
                <*> foo
                <*> foo
                <*> foo
                <*> foo
                etc

-}