-- | This module defines how the state changes
--   in response to time and user input
module Controllers.GameController (gameController) where

import Framework

import Models.State
import Views.GameView

gameController = Controller {
    fstep = baseStep,
    fevnt = event
}

clamp :: (Ord a) => a -> a -> a -> a
clamp mn mx = max mn . min mx

baseStep :: Float -> State -> IO (ControllerReturn State)
baseStep delta gstate@(LoadedState _ _ wrld) = do
    let sm       = mouse $ input gstate
    let view     = viewloc $ world gstate
    let cm       = screenToCoords sm
    let newloc   = (fst view + fst cm / 10, snd view + snd cm / 10)
    let clamped  = (clamp 0 (fromIntegral $ width $ tilegrid $ world gstate) $ fst newloc, clamp 0 (fromIntegral $ height $ tilegrid $ world gstate) $ snd newloc)
    let mousex   = fromIntegral (fst sm) / fromIntegral (fst $ screensize $ input gstate) * 2
    let mousey   = fromIntegral (snd sm) / fromIntegral (snd $ screensize $ input gstate) * 2
    newobjs <- mapM (\o -> gstep o o delta gstate) (objects wrld)
    let newstate = if (abs mousex < 0.9 && abs mousey < 0.9)
        then gstate
        else gstate {
            world = wrld {
                viewloc = clamped,
                objects = newobjs
            }
        }
    return $ In gameView $ Show newstate

event :: Event -> State -> IO (ControllerReturn State)
event delta@(EventMotion (x, y)) gstate = do
    newobjs <- mapM (\o -> gevent o o delta gstate) (objects $ world gstate)
    let newstate = gstate {
        input = (input gstate) { mouse = (round x, round y) },
        world = (world gstate) {
            objects = newobjs
        }
    }
    return $ In gameView $ Show newstate
event delta gstate = do
    newobjs <- mapM (\o -> gevent o o delta gstate) (objects $ world gstate)
    return $ In gameView $ Show (gstate {
        world = (world gstate) {
            objects = newobjs
        }
    })

