-- | This module defines how the state changes
--   in response to time and user input
module Controllers.BaseController (baseController) where

import Framework

import Models.State
import Views.BaseView
import Views.GameView
import System.IO(writeFile)

baseController = Controller {
    fstep = baseStep,
    fevnt = event
}

baseStep :: Float -> State -> IO (ControllerReturn State)
baseStep _ gstate = return $ Show gstate

event :: Event -> State -> IO (ControllerReturn State)
event _ gstate = do
    {-
    --writeFile "test.txt" $ unlines $ map save $ objects gstate
    x <- readFile "test.txt"
    let newstate2 = gstate {
        objects = [load x]
    } 
    putStrLn $ unlines $ map save $ objects newstate2 -}
    return $ In baseView $ Show gstate
