module Main where

import Framework

import Controllers.GameController
import Models.State
import Models.Unit
import Views.GameView

main :: IO ()
main = do
    files <- generateFiles
    let tf = tile files
    let gf = gui files
    {-let hud = HUD_element 6 2 [[cornerLeftUp   gf, ornRightTop    gf, borderUp   gf, borderUp   gf, ornLeftTop    gf, cornerRightUp   gf],
                               [cornerLeftDown gf, ornRightBottom gf, borderDown gf, borderDown gf, ornLeftBottom gf, cornerRightDown gf]] -}
    let hud_group1 = translate ((130 +) $ -1024 / 2) $ (-768 / 2) + 60
    let newhud = [HUDpiece $ hud_group1 $ hud1 gf, 
                  HUDpiece $ hud_group1 $ scale 0.4 0.5 $ translate (-210) (-40) $ color white $ Text "Barrack",
                  --
                  HUDpiece $ hud_group1 $ hud1 gf, 
                  HUDpiece $ hud_group1 $ scale 0.4 0.5 $ translate (-210) (-40) $ color white $ Text "Barrack"]
    let grid = TileGrid 7 6 [[dirt tf, stone tf, snow  tf, dirt tf, dirt tf, stone tf, snow tf],
                             [rock tf, snow  tf, dirt  tf, dirt tf, dirt tf, stone tf, snow tf],
                             [dirt tf, rock  tf, stone tf, dirt tf, dirt tf, stone tf, snow tf],
                             [dirt tf, rock  tf, stone tf, dirt tf, dirt tf, stone tf, snow tf],
                             [dirt tf, rock  tf, stone tf, dirt tf, dirt tf, stone tf, snow tf],
                             [dirt tf, dirt  tf, dirt  tf, dirt tf, dirt tf, stone tf, snow tf]]
    let picture = color yellow $ line [(x, y) | x <- [-5 .. 5], y <- [-5 .. 5]]
    let texture = basic $ units files
    play (InWindow "Game" (1024, 768) (0, 0)) -- Or FullScreen
            blue            -- Background color
            30               -- Frames per second
            gameController   -- Initial Controller
            LoadedState {
                file = files,
                input = InputState {
                    screensize = (1024, 768),
                    mouse = (0, 0)
                },
                world = WorldState {
                    tilegrid = grid,
                    hud  = newhud,
                    objects = [{-GObject { gstep = (\o _ _ -> return o), gevent = (\o _ _ -> return o), draw = picture, pos = Position 3 3 },-}
                     unit (3.0, 4.0) (-0.01, -0.01) (_E texture)],
                    viewloc = (0.5, 0.5)
                }
            }
