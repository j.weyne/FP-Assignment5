module Models.Object where

import Framework.Game.Types (
    GameObject, Position(..), Speed(..), 
    load, save)

--Containers
data Object = 
    Object {
        position :: Position,
        speed    :: Speed
    }
    deriving (Read, Show)

instance GameObject Object where
    load   = read
    save   = show

{- Object {position = getPos input, speed = getSpd input}
            where getPos = Position . head
                  getSpd = Speed . head . tail
                  input  = map read $ lines s-}