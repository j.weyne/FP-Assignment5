module Models.State (State(..), WorldState(..), InputState(..), Tile(..), TileGrid(..), GUI_element(..), HUD(..), GObject(..), Object(..), module Externals) where

import Framework

import Externals
import Models.Object

data State =
    BaseState {
        file  :: ExternalFiles,
        input :: InputState
    } |
    LoadedState {
        file  :: ExternalFiles,
        input :: InputState,
        world :: WorldState
    } deriving (Show)

type Tile        = Picture
type GUI_element = Picture
type HUDpiece    = Picture

data HUD        = 
    HUD_element {
        width_  :: Int,
        height_ :: Int,
        design :: [[GUI_element]]
    } |
    HUDpiece {
        piece :: Picture
    }
       deriving (Show)

data TileGrid   = TileGrid {
    width  :: Int,
    height :: Int,
    grid   :: [[Tile]] --column-first
} deriving (Show)

data InputState =
    InputState {
        screensize :: (Int, Int),
        mouse      :: (Int, Int)
    } deriving (Show)

data WorldState =
    WorldState {
        tilegrid :: TileGrid,
        hud      :: [HUD],
        objects  :: [GObject],
        viewloc  :: (Float, Float)
    } deriving (Show)

--Containers
data GObject = 
    GObject {
        gstep  :: GObject -> Float -> State -> IO GObject,
        gevent :: GObject -> Event -> State -> IO GObject,
        draw   :: Picture,
        pos    :: Position
    }

instance Show GObject where
    show = \s -> "not yet implemented"