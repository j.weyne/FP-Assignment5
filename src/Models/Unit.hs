module Models.Unit where

import Framework
import Models.State

unit          :: Position -> Speed -> Picture -> GObject
unit pos spd tex  = GObject {
        gstep = \o f s1 -> update o spd (clamp s1),
        gevent = \o e s2 -> inputHandler o e spd,
        draw = tex,
        pos = pos
    }
       where grid = tilegrid . world

update :: GObject -> Speed -> Point -> IO GObject
update o spd clamp = return $ o {
    pos = move clamp (pos o) spd
} 

inputHandler :: GObject -> Event -> Speed -> IO GObject
inputHandler o e spd_ = do
    case e of 
        EventKey (MouseButton RightButton) Up _ (x, y) -> do
            let dir = calcDirection (pos o) $ screenToCoords (round x, round y)
            let nspd = dir * spd_
            appendFile "E:/Repos/FP-Ass/error3.txt" ("\t" ++ (show nspd) ++ " : " ++ (show dir))      
            return $ o {
                  gstep = \o f s -> update o nspd (clamp s)
            }
        _ -> return o

--Pure functions
directionalTex :: State -> Speed -> Picture
directionalTex s (x, y) = case (x, y) of
    y > 0 && x > 0 -> $ unitModels $ file s

move   :: Point -> Position -> Speed -> Position
move clamp p spd = 
             let nx = (fst p) + (fst spd)
                 ny = (snd p) + (snd spd)
                 npos = (nx, ny)
             in case npos < clamp of
                    True  -> npos
                    False -> p

calcDirection    :: Position -> Position -> Speed
calcDirection p1 = normalizeV . (p1 -)

clamp :: State -> Point
clamp c = (fromIntegral $ width $ tilegrid $ world c, fromIntegral $ height $ tilegrid $ world c)
    
{-
import Framework.Game.Types (
    GameObject, InGame,
    load, save, step
    )
import Models.Object

--Types
type Name           = String
newtype HP          = HP        Float
    deriving (Read, Show)
newtype DPS         = DPS       Float
    deriving (Read, Show)

--Containers
data UnitType       = Air | Ground | Naval
    deriving (Read, Show)
data Unit        =
    Unit {
        object  :: Object,
        --texture :: Picture,
        uType   :: UnitType,
        name    :: Name,
        hp      :: HP,
        dps     :: DPS
    }
    deriving (Read, Show)

instance GameObject Unit where
    load    = undefined
    save    = undefined
{-
instance Drawable Unit where
    {-draw (Position { unPos = v })    = translate x y . texture
        where x = fst v
              y = snd v -}
    draw = undefined

instance InGame Unit where
    step    = undefined

instance Movable Unit where
    pos     = position . object
-} -}