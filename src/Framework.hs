module Framework (
        module Graphics.Gloss.Data.Color,
        module Graphics.Gloss.Game,
--
        module Framework.Types,
        module Framework.Graphics,
        module Framework.Game.Types,
--
        Display(..),
        play,
--
        screenToCoords,
        coordsToScreen
        ) where

--Gloss library modules
import Graphics.Gloss.Interface.IO.Game (Display(..), playIO)
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Game hiding (play)
--Framework modules
import Framework.Types
import Framework.Router
import Framework.Graphics
import Framework.Game.Types

--Re-export play with less verbose arguments
play display color fps controller model = playIO
    display
    color
    fps
    MVCState {
        model = model,
        view = mempty,
        controller = [controller]
    }
    routeView
    routeEvent
    routeStep

--OWN METHODS--
--Isometric coords to screen location
coordsToScreen :: (Float,Float) -> (Int,Int)
coordsToScreen (x, y) = (nx, ny)
    where
        nx = round $ (x - y) *  128
        ny = round $ (x + y) * (-64)

--Screen location to isometric coords
screenToCoords :: (Int,Int) -> (Float,Float)
screenToCoords (x, y) = (nx, ny)
    where
        nx = (ox /  128  + oy / (-64)) / 2
        ny = (oy / (-64) - ox /  128)  / 2
        ox = fromIntegral x
        oy = fromIntegral y